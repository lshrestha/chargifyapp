class DonationsController < ApplicationController

    def create
      # binding.pry
      @productFamily = Chargify::ProductFamily.find_by_handle('donationtest')
      if params[:email].nil?
         params[:email] = "asdf@asdf.com"
      end
      @customer = createCustomer(params)
      if @customer.id.present?
         params[:product_family_id] = @productFamily.id
         @product = createProduct(params)
      end

      if @product.id.present?
         @subscription = createSubscription(params)
        # binding.pry
      end
      redirect_to dashboard_thank_path(   :amount => @donation_amount, :recurring_frequency => @recurring_frequency)
    end

    def createCustomer(params)
      @customer = Chargify::Customer.create(:first_name=> params[:first_name], :last_name=>params[:last_name], :email=>params[:email])
    end

    def createProduct(params)
      @params = params

      params[:name] ||= "Donation by " + @customer.first_name
      params[:handle] ||= @customer.first_name.downcase()+ @customer.last_name.downcase()
      params[:price_in_cents] =  params[:donation_amount].to_f * 100 || "100"
      params[:interval] ||= "1"
      Chargify::Product.create( { "name" => params[:name], "handle" => params[:handle], "description" => params[:description], "accounting_code" => params[:accounting_code], "request_credit_card":true, "price_in_cents" => params[:price_in_cents], "interval" => params[:interval], "interval_unit":"month", "auto_create_signup_page": false ,"product_family_id" => params[:product_family_id]})
    end

    def createSubscription(params)
      # //for testing
      params[:expiration] = DateTime.now + 1.year
      params[:card_number] = 1

      Chargify::Subscription.create(
        :customer_id => @customer.id,
        :product_handle => @product.handle,
        :credit_card_attributes => {
          :first_name => params[:first_name],
          :last_name => params[:first_name],
          :expiration_month => params[:expiration].month,
          :expiration_year => params[:expiration].year,
          :full_number => params[:card_number]
        }
      )
    end



  end
