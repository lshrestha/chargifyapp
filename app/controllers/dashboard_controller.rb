class DashboardController < ApplicationController
  def home
  end

  def thank
  end

  def donate
    if params[:email] == params[:email_confirm]
      @user = User.create({:email=>params[:email], :password=>"password", :password_confirmation=>"password"})
    end
    @donation_amount = params[:donation_amounts][:predefined] || params[:donation_amounts][:custom_amount]
    @recurring_frequency = params[:recurring_frequency]
    redirect_to dashboard_give_path(   :amount => @donation_amount, :recurring_frequency => @recurring_frequency)
    # binding.pry

  end
end
