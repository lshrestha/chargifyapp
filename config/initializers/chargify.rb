CHARGIFY_CONFIG = YAML::load(File.read('config/chargify.yml'))[Rails.env].with_indifferent_access

Chargify.configure do |c|
  c.api_key = CHARGIFY_CONFIG[:api_key]
  c.subdomain = CHARGIFY_CONFIG[:subdomain]
end

# useful method
Chargify::Base.class_eval do
  def self.count
    find(:all).count
  end
end

