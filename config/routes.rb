Rails.application.routes.draw do

  get 'dashboard/thank'
  post 'dashboard/donate'
  get 'dashboard/give'

  root :to => "dashboard#home"

  resources :donations do
    collection do
      post 'create'
    end
  end

  devise_for :user
end
